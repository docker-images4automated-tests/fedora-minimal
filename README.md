# fedora-minimal

Upstream source of Red Hat Enterprise Linux. https://getfedora.org/

* https://registry.fedoraproject.org/repo/fedora-minimal/tags/
* [*Building Smaller Container Images*
  ](https://fedoramagazine.org/building-smaller-container-images/)
  2019-05 Muayyad Alsadi
